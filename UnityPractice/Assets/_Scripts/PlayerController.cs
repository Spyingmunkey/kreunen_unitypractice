﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float Speed;

    int count = 0;

    public Text countText;
    public Text winText;





	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= -5)
        {

            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);

        }
	}

    void RestartLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);


    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 Movement = new Vector3(moveHorizontal, 0f, moveVertical);


        rb.AddForce(Movement * Time.deltaTime * Speed);

    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Pickup"))
        { 

        count += 1;

        SetCountText();


        Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("SpeedZone"))
        {
            Speed = Speed * 2f;

        }
    }

    void OnTriggerExit(Collider other)
    {
       
        if (other.gameObject.CompareTag("SpeedZone"))
        {

            Speed = Speed / 2f;

        }



    }


    void SetCountText()
    {

        countText.text = "Count: " + count.ToString();


        if (count >= 12)
        {

            winText.text = "You Win!";
        }
    }


}
